var areas = {};

(function($) {

  areas.edited = false;
  areas.transactionId = 0;

  areas.init = function(picture, response) {
    var fileReader = new FileReader();
    fileReader.onload = function(e) {
      areas.picture = e.target.result;
      $('#picture-wrapper').append('<img src="' + areas.picture + '">');

      $('#process-text').fadeOut(400);
      $('#upload-image').fadeOut(400, 'swing', function() {
        $('#select-areas-text').fadeIn(400);
        $('#picture-wrapper').fadeIn(400);
        $('#areas-functions').fadeIn(400);
        areas.initAreas(picture, response);
      });
    };
    fileReader.readAsDataURL(picture);

    $('[data-toggle="tooltip"]').tooltip();
  };

  areas.initAreas = function(picture, response) {
    areas.transactionId = response['id'];
    areas.$selectImg = $('#picture-wrapper').find('img');

    var selectImgWidth = areas.$selectImg.width();
    var selectImgHeight = areas.$selectImg.height();

    var areasCount = response['areas'].length;
    for (var i = 0; i < areasCount; i++) {
      response['areas'][i]['x'] *= selectImgWidth / 100;
      response['areas'][i]['width'] *= selectImgWidth / 100;
      response['areas'][i]['y'] *= selectImgHeight / 100;
      response['areas'][i]['height'] *= selectImgHeight / 100;
    }

    areas.areas = response['areas'];

    areas.$selectImg.selectAreas({
      areas: areas.areas,
      allowEdit: false
    });

    $('#cancel-it-button, #start-over-button').on('click', function() {
      window.location = '';
    });
    $('#blur-it-button').on('click', function() {
      areas.blurIt();
    });
    $('#edit-it-button').on('click', function() {
      areas.initEdit();
    });
    $('#reset-it-button').on('click', function() {
      $('#edit-it-button').removeClass('hidden');
      $('#reset-it-button').addClass('hidden');

      areas.$selectImg.remove();
      $('#picture-wrapper').empty();
      $('#picture-wrapper').append('<img src="' + areas.picture + '">');

      $('#picture-wrapper').removeClass('editing');
      areas.edited = false;

      areas.$selectImg = $('#picture-wrapper').find('img');
      areas.$selectImg.selectAreas({
        areas: areas.areas,
        allowEdit: false
      });
    });
  };

  areas.initEdit = function() {
    areas.edited = true;

    areas.$selectImg.remove();
    $('#picture-wrapper').empty();
    $('#picture-wrapper').append('<img src="' + areas.picture + '">');
    areas.$selectImg = $('#picture-wrapper').find('img');
    $('#picture-wrapper').addClass('editing');
    $('#edit-it-button').addClass('hidden');
    $('#reset-it-button').removeClass('hidden');

    areas.$selectImg.selectAreas({
      areas: areas.areas,
      allowEdit: true,
      minSize: [60, 60]
    });

    $('#picture-wrapper').on('mouseleave', '> div', function() {
      areas.$selectImg.selectAreas('blurAll');
    });
  };

  areas.blurIt = function() {
    var ars = areas.$selectImg.selectAreas('areas');

    for (var i = 0; i < ars.length; i++) {
      ars[i].x = ars[i].x / $('#picture-wrapper img')[0].width;
      ars[i].y = ars[i].y / $('#picture-wrapper img')[0].height;
      ars[i].height = ars[i].height / $('#picture-wrapper img')[0].height;
      ars[i].width = ars[i].width / $('#picture-wrapper img')[0].width;
    }

    var dataString = {
      message: areas.transactionId
    };
    var jsonString = JSON.stringify(dataString);
    var dataString = {
      id: areas.transactionId,
      areas: ars,
      edited: areas.edited
    };
    var jsonString = JSON.stringify(dataString);
    $.ajax({
      type: "POST",
      url: "/api/blurring.php",
      data: {data : jsonString},
      cache: false,

      success: function(data){
        $('#edit-it-button').fadeOut(400);
        $('#reset-it-button').fadeOut(400);
        $('#cancel-it-button').fadeOut(400);
        $('#blur-it-button')
          .fadeOut(400, 'swing', function() {
          $('#download-button')
            .css('display', 'inline-block')
            .css('opacity', 0)
            .attr("download", "blurred-" + data['id'] + ".jpg")
            .attr("href", '/api/transactions/images/blurred/' + data['id'] + '.jpg')
            .animate({
              opacity: 1
            }, 200);
          $('#start-over-button').fadeIn(400);
        });
        $('#select-areas-text').fadeOut(400, 'swing', function() {
          $('#blurred-text').fadeIn(200);
        });
        $('#picture-wrapper').fadeOut(400, 'swing', function() {
          $('#picture-wrapper').empty();
          $('#picture-wrapper').append('<img src="/api/transactions/images/blurred/' + data['id'] + '.jpg">');
          $('#picture-wrapper').fadeIn(200);
        });
      }
    });
  };

} (jQuery));