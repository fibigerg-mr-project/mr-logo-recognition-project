//CORS middleware
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With, Cache-Control, Accept, Origin, X-Session-ID');
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,TRACE,COPY,LOCK,MKCOL,MOVE,PROPFIND,PROPPATCH,UNLOCK,REPORT,MKACTIVITY,CHECKOUT,MERGE,M-SEARCH,NOTIFY,SUBSCRIBE,UNSUBSCRIBE,PATCH');
  res.header('Access-Control-Allow-Credentials', 'false');
  res.header('Access-Control-Max-Age', '1000');

  next();
};

Dropzone.options.uploadImage = {
  paramName: "file", // The name that will be used to transfer the file
  maxFilesize: 20, // MB
  maxFiles: 1,
  addRemoveLinks: false,
  thumbnailHeight: 300,
  thumbnailWidth: null,
  previewTemplate: $('#preview-template').html(),
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With, Cache-Control, Accept, Origin, X-Session-ID, Access-Control-Allow-Origin',
    'Access-Control-Allow-Credentials': 'false'
  },
  init: function() {
    this.on('thumbnail', function(file, dataUrl) {
      $('#intro-text').animate({
        height: 0,
        opacity: 0
      }, 400);
      $('#process-text').fadeIn(600);
    });
    this.on('success', function(file, response) {
      areas.init(file, response);
    });
    this.on('uploadprogress', function(file, progress) {
      if (progress == 100) {
        $('.dz-preview .process-spinner').css('display', 'block').css('opacity', 0);
        $('.dz-progress').animate({
          opacity: 0
        }, 0, 'swing', function() {
          $('.dz-preview .process-spinner').animate({
            opacity: 1
          }, 200);
        });

      }
    });
  },
};