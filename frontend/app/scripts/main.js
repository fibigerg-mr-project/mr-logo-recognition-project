// jshint devel:true

(function($) {

  var web = {};

  // When page is ready.
  web.init = function() {

    $('#download-button').on('click', function() {
      $('#surveyModal').modal();
    });

    $('#submit-suvey').on('click', function() {
      $('#surveyModal').modal('hide');
      $('#surveySuccess').fadeIn(600);

      // TODO save survey
      //$.ajax({
      //  type: "POST",
      //  url: "http://www.logoremoval.local/api/survey.php",
      //  data: {data : jsonString},
      //  cache: false,
      //  success: function(data){
      //  }
      //});
    });

  };

  // When all resources are loaded.
  web.load = function() {
    $('body').removeClass('loading');
  };

  // When window resizes.
  web.resize = function() {

  };

  // Page load event where you can initialize values and call other initializers.
  $(document).ready(web.init);

  // All resources are loaded.
  $(window).load(web.load);

  // When window resizes.
  $(window).resize(web.resize);

} (jQuery));

// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
} (jQuery));