import os
import sys
import socket
import urllib
import urllib2
import json
import time

# Define directory
# Fill in what you want yourself
directoryBase = r"C:\Users\Erwin\Desktop\test\\"

# Define search term
searchInput = raw_input("Search Term: ")

def searchLogoImages(_directoryBase, _searchInput):
    # Replace spaces ' ' in search term for '%20' in order to comply with request
    searchTerm = (_searchInput).replace(' ','%20')
    
    #Define filetypes

    fileTypes = [".jpeg", ".jpg"]
    fileType = "jpeg"
    imgType = "photo"
    imgSize = "large"

    #Get user IP
    userIP = socket.gethostbyname(socket.gethostname())

    #Insert search term and user IP in request URL
    urlBase = ('https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=' +
            searchTerm +
            '&userip=' +
            userIP +
            '&as_filetype=' +
            fileType +
            '&imgType=' +
            imgType +
            '&imgSize=' +
            imgSize +
            '&rsz=6')

    #Print user IP
    print "IP address: " + userIP

    # Create directory if it does not exist yet.
    # Concurrency issue if after check it is created before we try to create it
    directory = _directoryBase + searchInput + "\\"

    if not os.path.exists(directory):
        os.makedirs(directory)
    
    # Get up to 30 results by requesting 5 pages of 6 results
    counter = 1    
    
    count = 0

    while counter <= 30:
        url = urlBase + '&start=' + str(count * 6)
        request = urllib2.Request(url, None, {'Referer': 'test'})
        response = urllib2.urlopen(request)

        # Get results using JSON
        results = json.load(response)
        data = results['responseData']
        dataInfo = data['results']

        # Iterate for each result and get unescaped url
        for myUrl in dataInfo:
            if counter <= 30:            
        
                #Create filename from url
                fileName = str(counter) +"_" + (myUrl['unescapedUrl'].split('/')[-1]).split('?')[0]
        
                #Checks if the url directly refers to an image
                if fileName != str(counter):
            
                    extension = False
            
                    # Checks if the file has an extension, otherwise we give it a .jpeg extension
                    # In most cases this will be fine
                    for fileType in fileTypes:
                        if fileName.endswith(fileType):
                            extension = True
            
                    if extension == False:
                        fileName += ".jpeg"
                
            
                    print myUrl['unescapedUrl']
                    print fileName
        
                    # Creates a connection and downloads the image
                    try:
                        resource = urllib2.urlopen(myUrl['unescapedUrl'], timeout=3)
                    except urllib2.HTTPError, e:
                        print "Oh dear... HTTPError: ", e.code
                        continue
                    except urllib2.URLError, e:
                        print "Oh dear... URLError."
                        continue
                    except socket.timeout:
                        print "Oh dear... Time-out error."
                        continue
                
                    output = open(directory + fileName,"wb")
                    output.write(resource.read())
                    output.close()
        
                    counter += 1
            else:
                continue

        # Sleep for one second to prevent IP blocking from Google
        time.sleep(1)
        count += 1
    
    #When done return 0
    return 0

#Run function
if __name__ == '__main__':
    searchLogoImages(directoryBase, searchInput)