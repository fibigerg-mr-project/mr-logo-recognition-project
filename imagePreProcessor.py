import os
import cv2
import numpy as np

def imagePreProcessor(imageFile, outPutFolder):
    # variable to limit the image size, namely, [Xmax, Ymax]
    imageThresholdValues = [640,640]
    
    #imageFile = r'C:\Users\JerryGuo\Desktop\test\3_pixar.jpg'
    
    img = cv2.imread(imageFile,0)
    axisToScale = findTheAxisToScale(img.shape)
    
    imgSizeX = int(img.shape[1])
    imgSizeY = int(img.shape[0])
    
    scaleRatio = getScaleRatio(axisToScale, img.shape, imageThresholdValues)
    
    tmpImg = img.copy()
    res = cv2.resize(tmpImg,(int(scaleRatio*imgSizeX), int(scaleRatio*imgSizeY)), interpolation = cv2.INTER_CUBIC)
    equ = cv2.equalizeHist(res)
    saveFileName = outPutFolder + "/" + os.path.basename(imageFile)
    
    cv2.imwrite(saveFileName, equ)
    print ("save file: " + saveFileName)
    
    '''
    cv2.imshow("Original", img)
    cv2.imshow("Scaled", res)
    
    k = cv2.waitKey(0)
    if(k == 27):
        cv2.destroyAllWindows()
    '''

def findTheAxisToScale(imageShape):
    if imageShape[1] > imageShape[0] or imageShape[1] == imageShape[0]:
        return "X"
    else:
        return "Y"

def getScaleRatio(axis, imageShape, threshold):
    if axis == 'X':
        activeSize = imageShape[1]
    else:
        activeSize = imageShape[0]
    
    if activeSize > threshold[0]:
        return float(threshold[0])/float(activeSize)
    else:
        return 1.0

sourceDir = "C:/Users/JerryGuo/Desktop/MMR/dataset/"
#sourceDir = "C:/Users/JerryGuo/Desktop/MMR/masks/"
targetDit = "C:/Users/JerryGuo/Desktop/MMR/dataset_small/"
#targetDit = "C:/Users/JerryGuo/Desktop/MMR/masks_small/"

for i in os.listdir(sourceDir):
    for j in os.listdir(sourceDir+i+"/test"):
        imageFileName = sourceDir+i+"/test/"+j
        outputFilePath = targetDit+i+"/test"
        print "Processing: "+imageFileName
        if os.path.isfile(imageFileName):
            imagePreProcessor(imageFileName,outputFilePath)
        #print "Saved to: "+outputFilePath
        