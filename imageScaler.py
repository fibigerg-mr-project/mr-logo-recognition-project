import cv2
import numpy as np

def imageScaler():
    # variable to limit the image size, namely, [Xmax, Ymax]
    imageThresholdValues = [500,500]
    
    imageFile = r'C:\Users\JerryGuo\Desktop\test\3_pixar.jpg'
    
    img = cv2.imread(imageFile)
    axisToScale = findTheAxisToScale(img.shape)
    
    imgSizeX = int(img.shape[1])
    imgSizeY = int(img.shape[0])
    
    scaleRatio = getScaleRatio(axisToScale, img.shape, imageThresholdValues)
    
    tmpImg = img.copy()
    res = cv2.resize(tmpImg,(int(scaleRatio*imgSizeX), int(scaleRatio*imgSizeY)), interpolation = cv2.INTER_CUBIC)
    
    cv2.imshow("Original", img)
    cv2.imshow("Scaled", res)
    
    k = cv2.waitKey(0)
    if(k == 27):
        cv2.destroyAllWindows()


def findTheAxisToScale(imageShape):
    if imageShape[1] > imageShape[0] or imageShape[1] == imageShape[0]:
        return "X"
    else:
        return "Y"

def getScaleRatio(axis, imageShape, threshold):
    if axis == 'X':
        activeSize = imageShape[1]
    else:
        activeSize = imageShape[0]
    
    if activeSize > threshold[0]:
        return float(threshold[0])/float(activeSize)
    else:
        return 1.0

imageScaler()