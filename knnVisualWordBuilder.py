from sklearn import metrics
from sklearn import datasets
from sklearn.cluster import KMeans

import cv2
import numpy as np

import Tkinter
import Tkconstants
import tkFileDialog

import pickle

import os
from glob import glob

class SIFT_dictionary:

    def __init__(self):
        pass

    def buildDictionary(self, siftArray, k):
        self.kmeans = KMeans(init='k-means++', n_clusters=k, n_init=10)
        self.kmeans.fit(siftArray)

    def predict(self, siftFeature):
        return self.kmeans.predict(siftFeature)

dirname = []
while len(dirname) < 1:
	dirname = tkFileDialog.askdirectory()


filenames = [y for x in os.walk(dirname) for y in glob(os.path.join(x[0], '*.jpg'))]

dess = []

sift = cv2.xfeatures2d.SIFT_create()

for i in range(0, len(filenames)):
    print(filenames[i])
    img = cv2.imread(filenames[i], 0)
    
    kp, des = sift.detectAndCompute(img,None)
    
    dess = dess + des.tolist()

print(len(dess))

siftDict = SIFT_dictionary()

siftDict.buildDictionary(dess, 128)

pickle.dump(siftDict, open("siftDictionary.data", "wb"))