import cv2
import numpy as np

import Tkinter
import Tkconstants
import tkFileDialog

filenames = []
while len(filenames) < 1:
    filenames = tkFileDialog.askopenfilenames()

results = []
kps = []
dess = []

#sift = cv2.SIFT()
sift = cv2.xfeatures2d.SIFT_create()
blabla = np.zeros((1,1))

for i in range(0, len(filenames)):
    img = cv2.imread(filenames[i], 0)
    #gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    kp, des = sift.detectAndCompute(img,None)

    img = cv2.drawKeypoints(img,kp, blabla,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    results.append(img)
    kps.append(kp)
    dess.append(des)


idx = 0
while(True):
    cv2.imshow(str(idx), results[idx])
    
    key = cv2.waitKey()

    if(key == 2555904 and idx < len(results) - 1):
        idx += 1
    elif(key == 2424832 and idx > 0):
        idx -= 1
    elif(key == 27):
        break
    cv2.destroyAllWindows()
cv2.destroyAllWindows()

#BFmatcher
bf = cv2.BFMatcher()

while(True):
    print("pls enter 2 numbers")
    input = raw_input()
    numbers = [int(s) for s in input.split() if s.isdigit()]

    if(len(numbers) > 1):
        num0 = numbers[0]
        num1 = numbers[1]
        if(num0 >= 0 and num1 >= 0 and num0 < len(results) and num1 < len(results)):
           matches = bf.knnMatch(dess[num0], dess[num1], k=2)

           good = []
           for m,n in matches:
               if m.distance < 0.75*n.distance:
                   good.append([m])

           
           img = cv2.drawMatchesKnn(results[num0], kps[num0], results[num1], kps[num1], good, blabla, flags=2)
           cv2.imshow("testing", img)
           cv2.waitKey(0)
           cv2.destroyAllWindows()


#for i in range(0, len(filenames)):
#    cv2.imshow('sift_keypoints.jpg',results[i])

cv2.destroyAllWindows()