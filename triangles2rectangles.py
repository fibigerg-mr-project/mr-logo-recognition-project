from Triangles import Triangle, MyPoint

# The fancy brilliant idea found online by Joost. Magically determines whether two rectangles overlap.
def rectangleOverlap(rec1, rec2):
    if (rec1[0]+rec1[2]<rec2[0]) or (rec2[0]+rec2[2]<rec1[0]) or (rec1[1]+rec1[3]<rec2[1]) or (rec2[1]+rec2[3]<rec1[1]):
        return False
    return True

# The function to convert each single triangle to a rectangle.
def convertTriangle2Rectangle(triangle):
    xValues = []
    yValues = []
    
    xValues.append(triangle.A.kps.pt[0])
    xValues.append(triangle.B.kps.pt[0])
    xValues.append(triangle.C.kps.pt[0])
    yValues.append(triangle.A.kps.pt[1])
    yValues.append(triangle.B.kps.pt[1])
    yValues.append(triangle.C.kps.pt[1])
        
    finalPoint = []
    
    finalPoint.append(min(xValues))
    finalPoint.append(min(yValues))
    finalPoint.append(max(xValues)-min(xValues))
    finalPoint.append(max(yValues)-min(yValues))
    
    return finalPoint

# The function to combine any numbers of rectangles into one rectangle, i.e. the biggest bounding-box, can be called using multiple rectangles as parameters.
def combineRectangles(*rec):
    xValues = []
    yValues = []
    
    for rectangle in rec:
        xValues.append(rectangle[0])
        xValues.append(rectangle[0]+rectangle[2])
        yValues.append(rectangle[1])
        yValues.append(rectangle[1]+rectangle[3])
    
    finalPoint = []
    
    finalPoint.append(min(xValues))
    finalPoint.append(min(yValues))
    finalPoint.append(max(xValues)-min(xValues))
    finalPoint.append(max(yValues)-min(yValues))
    
    return finalPoint

# The list to combine a list of rectangles into a big rectangle.
def combineRectList(rectangleList):
    if(len(rectangleList) == 0):
        return []
    finalRect = rectangleList[0]
    if(len(rectangleList) == 1):
        return finalRect
    for i in range(1, len(rectangleList)):
        finalRect = combineRectangles(finalRect, rectangleList[i])
    return finalRect

# The main function to convert a bunch of triangles into several geographically independent rectangles. Use simply by calling with a triangle list.
def triangles2rectangles(trianglesList):
    if len(trianglesList) == 0:
        return []
    
    rectanglesList = []
    
    for trianlge in trianglesList:
        rectangleUnit = convertTriangle2Rectangle(trianlge)
        rectanglesList.append(rectangleUnit)
    
    rectangleGroups = []
    
    for i in range(0, len(rectanglesList)):
        overlaps = []
        for j in range(len(rectangleGroups)):
            if(rectangleOverlap(rectangleGroups[j], rectanglesList[i])):
                overlaps.append(rectangleGroups[j])
        
        if(len(overlaps) == 0):
            rectangleGroups.append(rectanglesList[i])
        else:
            for overlap in overlaps:
                rectangleGroups.remove(overlap)
            overlaps.append(rectanglesList[i])
            rectangleGroups.append(combineRectList(overlaps))
    
    #print rectangleGroups
    return rectangleGroups

# A middle-ware class.
class keypoint:
    def __init__(self, p0):
        self.pt = p0
        self.angle = 0.0  

# If test.
if __name__ == '__main__':    
    kp0 = keypoint([3,11])
    kp1 = keypoint([3,6])
    kp2 = keypoint([7,11])
    t0 = Triangle(MyPoint(kp0, None), MyPoint(kp1, None), MyPoint(kp2, None), "", False)
    
    kp0 = keypoint([5,5])
    kp1 = keypoint([8,5])
    kp2 = keypoint([5,10])
    t1 = Triangle(MyPoint(kp0, None), MyPoint(kp1, None), MyPoint(kp2, None),  "", False)
    
    kp0 = keypoint([8,0])
    kp1 = keypoint([11,0])
    kp2 = keypoint([11,4])
    t2 = Triangle(MyPoint(kp0, None), MyPoint(kp1, None), MyPoint(kp2, None),  "", False)
    
    # ([3,11],[3,6],[7,11])
    # ([5,5],[8,5],[5,10])
    # ([8,0],[11,0],[11,4])
    # results:
    # [[3, 5, 5, 6], [8, 0, 3, 4]]
    
    trangList = [t0, t1, t2]
    
    a = triangles2rectangles(trangList)
    print a