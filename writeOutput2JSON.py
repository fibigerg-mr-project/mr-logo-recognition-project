# test data
testFilePath = "C:/Users/JerryGuo/Desktop/test/input.json"
rectangleList = [[0,0,1,1],[2,2,1,1],[4,4,1,1]]

# Build parameters to a dictionary
def buildJsonDictionary(rectList,inputImage, outputImage):
    tmpDict = {}
    
    tmpDict["image-input"] = inputImage
    tmpDict["image-output"] = outputImage
    
    recDictList = []
    for rectengle in rectList:
        tmpRectDict = {}
        tmpRectDict["x"] = rectengle[0]
        tmpRectDict["y"] = rectengle[1]
        tmpRectDict["width"] = rectengle[2]
        tmpRectDict["height"] = rectengle[3]
        recDictList.append(tmpRectDict)
    tmpDict["areas"] = recDictList
    
    return tmpDict

# The method to write output to a json file.
# The parameters are : rectangle list from triangles2rectangles.py, json file path, input image file path, 
# and the output image file path. The latter three are assigned a value according to API by default.
def writeOutput2JSON(rectangleList, jsonFilePath = "/transactions/recognition-output/1234.json", inputImageFile = "/images/uploads/the-UUID-1234.jpg", outputImageFile = "/images/uploads/the-UUID-1234.jpg"):
    jsonDictionary = buildJsonDictionary(rectangleList, inputImageFile, outputImageFile)
    
    jsonFile = open(jsonFilePath,'w+')
    
    jsonDictString = str(jsonDictionary)
    jsonFile.write(jsonDictString.replace("'", '"'))
    
    jsonFile.close()

if __name__ == '__main__':
    writeOutput2JSON(rectangleList, testFilePath)