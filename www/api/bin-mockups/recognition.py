import sys
import time
import json

def main():
    conf = sys.argv[1]

    with open(conf) as data_file:
        data = json.load(data_file)

    data['image_input']

    out = {}
    out['areas'] = list()
    out['areas'].append({
      'x': 10,
      'y': 20,
      'width': 10,
      'height': 30
    })
    out['areas'].append({
      'x': 70,
      'y': 60,
      'width': 20,
      'height': 10
    })

    with open(data['output_file'], 'w') as fp:
        json.dump(out, fp)

    time.sleep(1)

    print 0

if  __name__ =='__main__':main()