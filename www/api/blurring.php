<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Cache-Control, Accept, Origin, X-Session-ID, Access-Control-Allow-Origin, Access-Control-Allow-Credentials");

require_once './inc/HttpHandler.php';
require_once './inc/medoo.min.php';

define('UPLOADS_FOLDER', 'transactions/images/uploads');
define('BLURRED_FOLDER', 'transactions/images/blurred');
define('BLURRING_INPUT_FOLDER', 'transactions/blurring-input');

$http_handler = new HttpHandler();

// Initialize database.
$db = new medoo([
  'database_type' => 'mysql',
  'database_name' => 'logo_recognition',
  'server' => 'localhost',
  'username' => 'root',
  'password' => 'root',
  'charset' => 'utf8'
]);

try {

  // Store clients IP address.
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }

  // Decode input
  $data = json_decode(stripslashes($_POST['data']));
  $targetPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . UPLOADS_FOLDER . DIRECTORY_SEPARATOR;
  $targetFile =  $targetPath . $data->id . '.jpg';

  // TODO store edited and custom areas.

  // Define paths
  $blurring_input_filename = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . BLURRING_INPUT_FOLDER . DIRECTORY_SEPARATOR . $data->id . '.json';
  $blurring_output_image = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . BLURRED_FOLDER . DIRECTORY_SEPARATOR . $data->id . '.jpg';

  $output = new stdClass();
  $output->id = $data->id;
  $output->areas = $data->areas;
  $output->{'image-input'} = $targetFile;
  $output->{'image-output'} = $blurring_output_image;
  $output->edited = $data->edited;
  file_put_contents($blurring_input_filename, json_encode($output));

  $out = array();
  $blurring_status = exec('python ../bin/blurMain.py ' . $blurring_input_filename, $out);

  $http_handler->setData($output);
}
catch (Exception $e) {
  $http_handler->setResponseCode(500);
}

$http_handler->response();

