<?php

class HttpHandler {

  protected $responseCode = 200;
  protected $data;

  public function response() {
    if ($this->responseCode != 200) {
      $this->data['status'] = 'fail';
    }

    http_response_code($this->responseCode);
    header('Content-Type: application/json');
    print_r(json_encode($this->data));
  }

  /**
   * @param mixed $data
   */
  public function setData($data) {
    $this->data = $data;
  }

  /**
   * @param mixed $responseCode
   */
  public function setResponseCode($responseCode) {
    $this->responseCode = $responseCode;
  }

}