<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Cache-Control, Accept, Origin, X-Session-ID, Access-Control-Allow-Origin, Access-Control-Allow-Credentials");

require_once './inc/HttpHandler.php';
require_once './inc/medoo.min.php';

define('UPLOADS_FOLDER', 'transactions/images/uploads');
define('RECOGNITION_INPUT_FOLDER', 'transactions/recognition-input');
define('RECOGNITION_OUTPUT_FOLDER', 'transactions/recognition-output');

$http_handler = new HttpHandler();

// Initialize database.
$db = new medoo([
  'database_type' => 'mysql',
  'database_name' => 'logo_recognition',
  'server' => 'localhost',
  'username' => 'root',
  'password' => 'root',
  'charset' => 'utf8'
]);

try {

  // Store clients IP address.
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }

  // Store transaction in DB and retrieve ID.
  $filename = $_FILES['file']['name'];
  if (!$filename) {
    exit();
  }
  $id = $db->insert('transaction', [
    'filename' => (string) $filename,
    'created' => time(),
    'ip' => (string) $ip
  ]);

  // Store file
  $tempFile = $_FILES['file']['tmp_name'];
  $targetPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . UPLOADS_FOLDER . DIRECTORY_SEPARATOR;
  $targetFile =  $targetPath . $id . '.jpg';
  move_uploaded_file($tempFile, $targetFile);

  // Get logo areas.
  $recognition_input_filename = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . RECOGNITION_INPUT_FOLDER . DIRECTORY_SEPARATOR . $id . '.json';
  $recognition_output_filename = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . RECOGNITION_OUTPUT_FOLDER . DIRECTORY_SEPARATOR . $id . '.json';

  $output = new stdClass();
  $output->id = $id;
  $output->image_input = $targetFile;
  $output->output_file = $recognition_output_filename;
  file_put_contents($recognition_input_filename, json_encode($output));

  $recognition_status = exec('python ./bin-mockups/recognition.py ' . $recognition_input_filename);
  $recognition_output = json_decode(file_get_contents($recognition_output_filename)); // $recognition_output['areas']
  $recognition_output->id = $id;

  // TODO: Load logo information and append them to output.

  $http_handler->setData($recognition_output);
}
catch (Exception $e) {
  $http_handler->setResponseCode(500);
}

$http_handler->response();

