import sys
import time
import json
import regionBlurPixels

def main():
    conf = sys.argv[1]

    with open(conf) as data_file:
        data = json.load(data_file)

    regionBlurPixels.blurRegionPixels(data["image-input"], data["image-output"], data["areas"])

    print 0

if  __name__ =='__main__':
    main()