## @module regioo BlurPixels.py
#  Documentation for this module.
#  Module creator: Jerry Guo
#  This module is part of the MR- Logo Recognition Project, and handles blur-ish stuff in the project.
#  Input is a file name, which is pre-assigned according to API, and several list of blur regions / areas.
#  The blur region / area list contains four parameters, namely, [X, Y, Width, Height].
#  Note that the region should be within the image size, i.e., the left-up conner and the right-bottom conner of the region / area should be within the image frame.

import cv2
import os, types

##Input file name, modify as your wish, the second is the name in the API.
# imageFileName = r'C:\Users\JerryGuo\Desktop\test\3_pixar.jpg'
# @var imageFileName
# The default image file name for "image-input".
#imageFileName = "/images/uploads/the-UUID-1234.jpg"

# @var blurRectangle01, blurRectangle02
# Blur region inputs as a list, [X, Y, Width, Height], modify as your wish.
# Corresponds to "areas" in the API.
# Remember though that the length of the list has to be 4,and Width, Height > 0.
#blurRectangle01 = [280,60,160,55]
#blurRectangle02 = [290,180,150,95]
#
# TODO: document that we are using relative values not pixels and blurRects is a list with maps from select areas, not list with 4 elements

##The function to do the blurring, where parameter imgFile is the input file,
# and parameter blurRect is the region list with X and Y coordinates indicating
# the region to be blurred
def blurRegionPixels(imgFile, saveFileName, blurRects):
    ##if file exist, read the file, else return error
    if(os.path.isfile(imgFile)):
        # @var img
        # The input image.
        img = cv2.imread(imgFile)
    else:
        print "No such file!"
        return

    ##check sizes, input blur region should be within the image size, i.e., X < imgSizeX, Y < imgSizeY, X + Width < imgSizeX, Y + Height < imgSizeY
    # @var imgSizeX
    # The image size x.
    imgSizeX = int(img.shape[1])

    # @var imgSizeY
    # The image size y
    imgSizeY = int(img.shape[0])

    # @var finalImg
    # Make a copy of img to final image
    finalImg = img.copy()

    # @var fullBlurImg
    # Make a copy of img to a tmp image to be fully blurred
    fullBlurImg = img.copy()

    # The fancy Gaussian Blur!
    fullBlurImg = cv2.GaussianBlur(fullBlurImg, (111,111), 2000.0)

    # Check if parameters are correct.
    # Error for no blur region data.
    if( len(blurRects) == 0):
        print "Please specify at least one region to blur!"
        return

    # Parse input *args
    for blurRect in blurRects:
        # Error for input data type and list size.
        #if(type(blurRect) is types.ListType):
        # Convert relative values to pixels
        Xmin = int(blurRect['x'] * imgSizeX)
        Width = int(blurRect['width'] * imgSizeX)

        Ymin = int(blurRect['y'] * imgSizeY)
        Height = int(blurRect['height'] * imgSizeY)

        Xmax = Xmin + Width
        Ymax = Ymin + Height

        # Error for image size overflow.
        if((Xmin > imgSizeX) or (Xmax > imgSizeX) or (Ymin > imgSizeY) or (Ymax > imgSizeY)):
            print "Wrong blur region parameter input!"
            return
        #else:
        #    print "Wrong blur region parameter input!"
        #    return

        # @var regionImg
        # Extract a region that is to be blurred.
        regionImg = fullBlurImg[Ymin:Ymax, Xmin:Xmax]

        # Merge the blurred image back to the original image to get a final image.
        finalImg[Ymin:Ymax, Xmin:Xmax] = regionImg

    ##Show the blurred image using OpenCV.
    """
    cv2.imshow('Orz:=Final Blurred Image Output',finalImg)
    k = cv2.waitKey(0)
    if(k == 27):
        cv2.destroyAllWindows()
    """

    ##Or save the image to a file. The 2nd name is the name in the API.
    # saveFileName = os.path.dirname(imgFile)+"\\blurredOutput.jpg"
    # @var saveFileName
    # The default image file name for "image-output".
    #saveFileName = "/images/uploads/the-UUID-1234.jpg"
    cv2.imwrite(saveFileName, finalImg)
    return 1

##Run the function.
#if __name__ == '__main__':
#    blurRegionPixels(imageFileName, blurRectangle01, blurRectangle02)